﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Tangine;

using Sulakore.Habbo;
using Sulakore.Protocol;
using Sulakore.Communication;
using ElmerFudd.GUI.Messages;

namespace ElmerFudd.GUI
{
    public partial class FrmMain : ExtensionForm
    {
        public PacketCollection Packets;
        public WalkingEngine Walking;
        public TrackingEngine Tracking;

        public override bool IsRemoteModule => true;

        public FrmMain()
        {
            InitializeComponent();

            Packets = new PacketCollection(this);
            Triggers.InAttach(Packets.In.RoomLoading, RoomLoading);
        }

        public string Username;
        public string RoomName;
        public int RoomId;
        public bool Start = true;
        public bool EnableTracking = true;
        public bool EnableWalk = true;
        public bool EnablePersonality = true;
        public bool EnableCracking = true;

        private void FrmMain_Load(object sender, EventArgs e)
        {
        }

        private void RoomLoading(DataInterceptedEventArgs e)
        {
            e.Packet.ReadString();
            RoomId = e.Packet.ReadInteger();

            Triggers.InDetach();

            Walking = new WalkingEngine(this);
            Tracking = new TrackingEngine(this);

            Triggers.InAttach(Packets.In.UserPoof, Tracking.UserPoof);
            Triggers.InAttach(Packets.In.EntityAction, Tracking.EntityAction);
            Triggers.InAttach(Packets.In.EntityLoad, Tracking.EntityLoad);
            Triggers.InAttach(Packets.In.FurnitureDrop, Tracking.FurnitureDrop);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            Username = txtUsername.Text;
            RoomName = cmbRoom.Text;
            //EnableTracking = chkEnableTracking.Checked;
            //EnableWalk = chkEnableWalk.Checked;
            //EnablePersonality = chkEnablePersonality.Checked;
            //EnableCracking = chkEnableCracking.Checked;

            //Start = true;
        }
    }
}