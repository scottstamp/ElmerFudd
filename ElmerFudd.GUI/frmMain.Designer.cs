﻿namespace ElmerFudd.GUI
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUsername = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.cmbRoom = new System.Windows.Forms.ComboBox();
            this.lblRoom = new System.Windows.Forms.Label();
            this.chkEnableWalk = new System.Windows.Forms.CheckBox();
            this.chkEnablePersonality = new System.Windows.Forms.CheckBox();
            this.chkEnableTracking = new System.Windows.Forms.CheckBox();
            this.chkEnableCracking = new System.Windows.Forms.CheckBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Location = new System.Drawing.Point(12, 15);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(58, 13);
            this.lblUsername.TabIndex = 0;
            this.lblUsername.Text = "Username:";
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(76, 12);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(146, 20);
            this.txtUsername.TabIndex = 1;
            this.txtUsername.Text = "opsofar1";
            // 
            // cmbRoom
            // 
            this.cmbRoom.FormattingEnabled = true;
            this.cmbRoom.Items.AddRange(new object[] {
            "Club NX",
            "Picnic",
            "Welcome Lounge",
            "Coffee House"});
            this.cmbRoom.Location = new System.Drawing.Point(76, 38);
            this.cmbRoom.Name = "cmbRoom";
            this.cmbRoom.Size = new System.Drawing.Size(146, 21);
            this.cmbRoom.TabIndex = 2;
            this.cmbRoom.Text = "Club NX";
            // 
            // lblRoom
            // 
            this.lblRoom.AutoSize = true;
            this.lblRoom.Location = new System.Drawing.Point(12, 41);
            this.lblRoom.Name = "lblRoom";
            this.lblRoom.Size = new System.Drawing.Size(38, 13);
            this.lblRoom.TabIndex = 3;
            this.lblRoom.Text = "Room:";
            // 
            // chkEnableWalk
            // 
            this.chkEnableWalk.AutoSize = true;
            this.chkEnableWalk.Checked = true;
            this.chkEnableWalk.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkEnableWalk.Location = new System.Drawing.Point(12, 88);
            this.chkEnableWalk.Name = "chkEnableWalk";
            this.chkEnableWalk.Size = new System.Drawing.Size(101, 17);
            this.chkEnableWalk.TabIndex = 5;
            this.chkEnableWalk.Text = "Enable &Walking";
            this.chkEnableWalk.UseVisualStyleBackColor = true;
            // 
            // chkEnablePersonality
            // 
            this.chkEnablePersonality.AutoSize = true;
            this.chkEnablePersonality.Checked = true;
            this.chkEnablePersonality.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkEnablePersonality.Location = new System.Drawing.Point(12, 111);
            this.chkEnablePersonality.Name = "chkEnablePersonality";
            this.chkEnablePersonality.Size = new System.Drawing.Size(149, 17);
            this.chkEnablePersonality.TabIndex = 6;
            this.chkEnablePersonality.Text = "Enable &Personality Engine";
            this.chkEnablePersonality.UseVisualStyleBackColor = true;
            // 
            // chkEnableTracking
            // 
            this.chkEnableTracking.AutoSize = true;
            this.chkEnableTracking.Location = new System.Drawing.Point(12, 65);
            this.chkEnableTracking.Name = "chkEnableTracking";
            this.chkEnableTracking.Size = new System.Drawing.Size(104, 17);
            this.chkEnableTracking.TabIndex = 4;
            this.chkEnableTracking.Text = "Enable &Tracking";
            this.chkEnableTracking.UseVisualStyleBackColor = true;
            // 
            // chkEnableCracking
            // 
            this.chkEnableCracking.AutoSize = true;
            this.chkEnableCracking.Checked = true;
            this.chkEnableCracking.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkEnableCracking.Location = new System.Drawing.Point(12, 134);
            this.chkEnableCracking.Name = "chkEnableCracking";
            this.chkEnableCracking.Size = new System.Drawing.Size(126, 17);
            this.chkEnableCracking.TabIndex = 7;
            this.chkEnableCracking.Text = "Enable &Egg Cracking";
            this.chkEnableCracking.UseVisualStyleBackColor = true;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(12, 157);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(210, 23);
            this.btnStart.TabIndex = 8;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 192);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.chkEnableCracking);
            this.Controls.Add(this.chkEnableTracking);
            this.Controls.Add(this.chkEnablePersonality);
            this.Controls.Add(this.chkEnableWalk);
            this.Controls.Add(this.lblRoom);
            this.Controls.Add(this.cmbRoom);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.lblUsername);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmMain";
            this.Text = "ElmerFudd";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.ComboBox cmbRoom;
        private System.Windows.Forms.Label lblRoom;
        private System.Windows.Forms.CheckBox chkEnableWalk;
        private System.Windows.Forms.CheckBox chkEnablePersonality;
        private System.Windows.Forms.CheckBox chkEnableTracking;
        private System.Windows.Forms.CheckBox chkEnableCracking;
        private System.Windows.Forms.Button btnStart;
    }
}

