﻿using System.Collections.Generic;

namespace ElmerFudd.GUI.Messages
{
    public class PacketCollection
    {
        private FrmMain Main;
        public Incoming In;
        public Outgoing Out;
        //public Dictionary<Incoming, ushort> In = new Dictionary<Incoming, ushort>();
        //public Dictionary<Outgoing, ushort> Out = new Dictionary<Outgoing, ushort>();

        public PacketCollection(FrmMain main)
        {
            Main = main;

            In = new Incoming();
            In.UserPoof = Get("7255b52b747d3929979a6e69abb1b60a");
            In.EntityAction = Get("45d53173f4bf410c6f0d57f0fb0edca3");
            In.EntityLoad = Get("9bc4789617fc483c6bf739ab2f8e8419");
            In.FurnitureDrop = Get("e4e3bc19857c5495fcdcff4f36d17d3d");
            In.RoomLoading = Get("6baee118ced0e46a3ca5384b83e6338c");

            Out = new Outgoing();
            Out.UseItem = Get("1d783bdbfb54f51403c1f40d931d3043");
            Out.Walk = Get("5dec6a7881d4a598d5b15d0e743bcdcb");
            Out.Sit = Get("652fb85d3b1aa28f1824d234f960f799");
            Out.Wave = Get("5ba7e2b2897b1cca6988574139469a1b");
            Out.SpeechBubble = Get("c921caee95f07c16d63cd942e2cbff9d");
        }

        private ushort Get(string hash)
        {
            return Main.Game.GetMessageHeaders(hash)[0];
        }
    }

    public class Incoming
    {
        public ushort UserPoof,       // 7255b52b747d3929979a6e69abb1b60a
               EntityLoad,            // 9bc4789617fc483c6bf739ab2f8e8419
               EntityAction,          // 45d53173f4bf410c6f0d57f0fb0edca3
               FurnitureDrop,         // e4e3bc19857c5495fcdcff4f36d17d3d
               RoomLoading
            ;
    }

    public class Outgoing
    {
        public ushort UseItem,        // 1d783bdbfb54f51403c1f40d931d3043
               Walk,           // 5dec6a7881d4a598d5b15d0e743bcdcb
               Sit,            // 652fb85d3b1aa28f1824d234f960f799
               Wave,           // 5ba7e2b2897b1cca6988574139469a1b
               SpeechBubble    // c921caee95f07c16d63cd942e2cbff9d
            ;
    }
}
