﻿using ElmerFudd.GUI;
using ElmerFudd.GUI.Messages;
using Sulakore.Communication;
using Sulakore.Habbo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ElmerFudd
{
    public class WalkingEngine
    {
        private FrmMain Main;
        private PacketCollection Packets;
        private Random RandomGenerator;
        private Dictionary<int, List<int[]>> RandomCoordinates;

        public WalkingEngine(FrmMain main)
        {
            Main = main;
            Packets = Main.Packets;

            RandomGenerator = new Random();
            RandomCoordinates = new Dictionary<int, List<int[]>>();

            GenerateCoords(69118541, 9, 13, 14, 15);
            GenerateCoords(70616906, 15, 18, 16, 19);
            GenerateCoords(69118542, 13, 18, 18, 21);
            GenerateCoords(69118545, 8, 11, 16, 19);
        }

        public void WalkRandom()
        {
            var coordList = RandomCoordinates[Main.RoomId];
            var coord = coordList[RandomGenerator.Next(coordList.Count)];
            Walk(coord[0], coord[1], true);
        }

        public void WalkToBunny(HEntity targetEntity)
        {
            Walk(targetEntity.Tile.X, targetEntity.Tile.Y, false);
        }

        public void Walk(int x, int y, bool random)
        {
            int count = 0;
            while (Main.EnableWalk && count < 30)
            {
                HPoint[] validTiles = getValidTiles(x, y, random).ToArray();

                if (validTiles.Length == 0)
                    break;

                if (validTiles.Any(tile => IsTileEqual(Main.Tracking.Me.Tile, tile)))
                    return;

                foreach (HPoint validTile in validTiles)
                {
                    Main.Connection.SendToServerAsync(Packets.Out.Walk, validTile.X, validTile.Y);
                    Thread.Sleep(100);

                    if (validTiles.Any(tile => IsTileEqual(Main.Tracking.Me.Tile, tile)))
                        return;

                    count++;
                }

                Thread.Sleep(200);
                count++;
            }
        }

        private void GenerateCoords(int roomId, int xMin, int xMax, int yMin, int yMax)
        {
            var coordList = new List<int[]>();
            for (int x = xMin; x < xMax + 1; x++)
                for (int y = yMin; y < yMax + 1; y++)
                    coordList.Add(new int[] { x, y });

            RandomCoordinates.Add(roomId, coordList);
        }

        private List<HPoint> getValidTiles(int x, int y, bool random)
        {
            var validTiles = new List<HPoint>();

            validTiles.Add(new HPoint(x - 1, y)); // Middle Left
            validTiles.Add(new HPoint(x, y)); // Middle
            validTiles.Add(new HPoint(x + 1, y)); // Middle Right

            if (!random)
            {
                validTiles.Add(new HPoint(x - 1, y - 1)); // Top Left
                validTiles.Add(new HPoint(x, y - 1)); // Top Middle
                validTiles.Add(new HPoint(x + 1, y - 1)); // Top Middle

                validTiles.Add(new HPoint(x - 1, y + 1)); // Bottom Left
                validTiles.Add(new HPoint(x, y + 1)); // Bottom Middle
                validTiles.Add(new HPoint(x + 1, y + 1)); // Bottom Right
            }

            return validTiles;
        }

        private bool IsTileEqual(HPoint left, HPoint right)
        {
            return (left.X == right.X && left.Y == right.Y);
        }
    }
}
