﻿using ElmerFudd.GUI.Messages;
using Sulakore.Communication;
using Sulakore.Habbo;
using Sulakore.Protocol;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ElmerFudd.GUI
{
    public class TrackingEngine
    {
        private FrmMain Main;
        private PacketCollection Packets;
        private WalkingEngine Walking;

        private Task _walkTowardsBunnyTask;
        private string Username;

        public Dictionary<int, int> PoofCount;
        public Dictionary<int, HEntity> Entities { get; }
        public HEntity Me { get; set; }

        private HEntity _targetEntity;
        public HEntity TargetEntity
        {
            get { return _targetEntity; }
            set
            {
                _targetEntity = value;
            }
        }

        public TrackingEngine(FrmMain main)
        {
            Main = main;
            Packets = Main.Packets;
            Walking = Main.Walking;

            Username = Main.Username;
            Entities = new Dictionary<int, HEntity>();
            PoofCount = new Dictionary<int, int>();
        }

        public void UserPoof(DataInterceptedEventArgs e)
        {
            if (Main.Start)
            {
                HMessage packet = e.Packet;
                int virtualId = packet.ReadInteger();
                int poofType = packet.ReadInteger();

                if (Entities.TryGetValue(virtualId, out HEntity entity))
                {
                    if (poofType == 187 && TargetEntity == null) // Appear
                    {
                        TargetEntity = entity;
                        if (_walkTowardsBunnyTask?.IsCompleted ?? true)
                        {
                            if (PoofCount[virtualId] > 2)
                            {
                                _walkTowardsBunnyTask = Task.Factory.StartNew(() =>
                                    Walking.WalkToBunny(TargetEntity),
                                    TaskCreationOptions.LongRunning);
                            }
                        }
                    }
                    else if (poofType == 108) // Disappear
                    {
                        PoofCount[virtualId]++;
                        TargetEntity = null;
                    }
                }
            }
        }

        public void FurnitureDrop(DataInterceptedEventArgs e)
        {
            if (Main.Start)
            {
                HMessage packet = e.Packet;
                int furniId = packet.ReadInteger();

                Task.Factory.StartNew(() =>
                {
                    for (int i = 0; i < 100; i++)
                    {
                        Main.Connection.SendToServerAsync(Packets.Out.UseItem, furniId, 0);
                        Thread.Sleep(50);
                    }

                    Walking.WalkRandom();
                });
            }
        }

        public void EntityLoad(DataInterceptedEventArgs e)
        {
            if (Main.Start)
            {
                var entities = HEntity.Parse(e.Packet);
                foreach (HEntity entity in entities)
                {
                    Entities[entity.Index] = entity;
                    if (entity.Name == "Easter Bunny")
                        PoofCount.Add(entity.Index, 0);

                    if (entity.Name == Main.Username)
                        Me = entity;
                }
            }
        }

        public void EntityAction(DataInterceptedEventArgs e)
        {
            if (Main.Start)
            {
                foreach (var action in HEntityAction.Parse(e.Packet))
                {
                    if (Entities.Keys.Contains(action.Index))
                    {
                        Entities[action.Index].Tile = action.Tile;
                        if (action.Index == Me?.Index)
                            Me.Tile = action.Tile;
                    }
                }
            }
        }
    }
}
